import sys
from PyQt6.QtWidgets import *

class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setGeometry(50,50,300,450)
        self.setWindowTitle("First App")
        print("lesson 3 ",
              'https://coursehunter.net/course/programmirovanie-gui-na-python-s-ispolzovaniem-pyqt5?lesson=3)')

        self.show()

App=QApplication(sys.argv)
window=Window()
sys.exit(App.exec())